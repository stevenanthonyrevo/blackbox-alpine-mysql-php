# Alpine MySQL Docker #

[![Docker Pulls](https://img.shields.io/docker/pulls/scalified/alpine-mysql.svg)](https://hub.docker.com/r/scalified/alpine-mysql)
[![](https://images.microbadger.com/badges/image/scalified/alpine-mysql.svg)](https://microbadger.com/images/scalified/alpine-mysql)
[![](https://images.microbadger.com/badges/version/scalified/alpine-mysql.svg)](https://microbadger.com/images/scalified/alpine-mysql)

## Description

This repository is used for building a [**Docker**](https://www.docker.com) image containing [**MySQL**](https://www.mysql.com/) running on [**Alpine Linux**](https://alpinelinux.org/)

## Dockerhub

**`docker pull scalified/alpine-mysql`**

```
docker build -t localhost/alpine-mysql-php-supervisor:14.6 .
docker run -it localhost/alpine-mysql-php-supervisor:14.6 /bin/bash
ps aux
supervisord -c /etc/supervisord.conf
```

```
docker run -it -p 3306:3306 -p 8080:8080 localhost/alpine-mysql-php-supervisor:14.6 /bin/bash -c "supervisord -c /etc/supervisord.conf"
```

```
ENV SUPERVISOR_CONF_DIR=/etc/supervisor.d

ENV MYSQL_DATA_DIR=/opt/mysql/data
ENV MYSQL_USER=mysql
ENV MYSQL_STDOUT_FILE=/var/log/mysql-stdout.log
ENV MYSQL_STDERR_FILE=/var/log/mysql-stderr.log
```

[ref](https://github.com/Scalified/docker-alpine-mysql/blob/master/Dockerfile) 

```

mysql  Ver 15.1 Distrib 10.4.12-MariaDB, for Linux (x86_64) using readline 5.1

Connection id:          9
Current database:
Current user:           root@localhost
SSL:                    Not in use
Current pager:          stdout
Using outfile:          ''
Using delimiter:        ;
Server:                 MariaDB
Server version:         10.4.12-MariaDB MariaDB Server
Protocol version:       10
Connection:             Localhost via UNIX socket
Server characterset:    utf8mb4
Db     characterset:    utf8mb4
Client characterset:    utf8
Conn.  characterset:    utf8
UNIX socket:            /run/mysqld/mysqld.sock


```

## Version

| #      | Version |
|--------|---------|
| Alpine | 3.4     |

## Volumes

**`/opt/mysql/data`**

### How-To

#### Building Docker Image

`docker build . -t <tag>`

#### Running Docker Image

* Pulling from **Dockerhub**:  
  `docker run -it scalified/alpine-mysql /bin/sh`

* Launching the built image with <tag> tag:  
  `docker run -it <tag> /bin/sh`

## Scalified Links

* [Scalified](http://www.scalified.com)
* [Scalified Official Facebook Page](https://www.facebook.com/scalified)
* <a href="mailto:info@scalified.com?subject=[Squash TM Docker Image]: Proposals And Suggestions">Scalified Support</a>
