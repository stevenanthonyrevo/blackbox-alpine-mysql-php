#!/bin/bash

UBUNTU_SLEEP="/snap/bin/sleep"
UBUNTU_DOCKER="/snap/bin/docker"
MACOS_DOCKER="/bin/docker"
MACOS_SLEEP="/bin/sleep"

HOST=localhost
NAME=alpine-mysql-php-supervisor
VERSION=14.6
IMAGE=$HOST/$NAME:$VERSION 
PORT=8080


builder() {
#build script to generate tar file of dir. 
mkdir dist
cd .. 
tar -czvf build.tar.gz .   
mv build.tar.gz ./bin/dist/build-packaged.tar.gz
cd ./bin/dist 
tar xvzf build-packaged.tar.gz 
rm -rf build-packaged.tar.gz
rm -rf bin 
}

snooze() {
# use Ubuntu Installed Sleep 	
if [ -x $UBUNTU_SLEEP ]; then 
	sleep 3 >/dev/null 2>&1
   else
       # use Mac Os Installed Sleep 
       if [ -x $MACOS_SLEEP ]; then
       	 sleep 3 >/dev/null 2>&1 
       fi
fi 
}

# user Ubuntu Installed Docker 
if [ -x $UBUNTU_DOCKER ]; then 
	snooze >&2
	docker ps >&2
	builder >&2 
	snooze >&2 
	docker build -t "$IMAGE" . >&2
        snooze >&2
	docker stop $NAME-v$VERSION >&2 
        docker rm $NAME-v$VERSION >&2 	
        docker run --name "$NAME-v$VERSION" -it -p 3306:3306 -p $PORT:8080 "$IMAGE" /bin/bash -c "supervisord -c /etc/supervisord.conf"	
	exit 1 
    else 
	# use Mac OS Installed Docker
	if [ -x $MACOS_DOCKER ]; then
	   snooze >&2 
	   docker ps >&2
	   builder >&2 
	   snooze >&2 
	   docker build -t "$IMAGE" . >&2
           snooze >&2 
	   docker stop $NAME-v$VERSION >&2
           docker rm $NAME-v$VERSION >&2
           docker run --name "$NAME-v$VERSION" -it -p 3306:3306 -p $PORT:8080 "$IMAGE" /bin/bash -c "supervisord -c /etc/supervisord.conf"
	   exit 1 
	else 
           echo 'failed to find any docker, can not run on system. Please install docker to path: /bin/docker or /snap/docker' >&2
	   exit 1 
	fi
fi


# su - root -c "docker ps"

# docker build -t "$IMAGEv$VERSION" . 

# docker run --name $NAME-v$VERSION -p $PORT:8080 -v "$PATH/codebase:/var/www/html" "$IMAGEv$VERSION"

# docker ps -aq --filter name=$NAME-v$VERSION 

# docker run -it -p 3306:3306 -p 8080:8080 localhost/alpine-mysql-php-supervisor:14.6 /bin/bash -c "supervisord -c /etc/supervisord.conf"

# echo $HOST
# echo $NAME
# echo $VERSION 
# echo $IMAGE 

