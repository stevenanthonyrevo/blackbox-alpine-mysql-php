FROM alpine:3.11

ENV SUPERVISOR_CONF_DIR=/etc/supervisor.d
ENV MYSQL_DATA_DIR=/opt/mysql/data
ENV MYSQL_USER=mysql
ENV MYSQL_STDOUT_FILE=/var/log/mysql-stdout.log
ENV MYSQL_STDERR_FILE=/var/log/mysql-stderr.log

RUN apk add --update --no-cache \
	supervisor \
	mysql \
	mysql-client
RUN apk --no-cache add php7 php7-fpm php7-mysqli php7-json php7-openssl php7-curl \
    php7-zlib php7-xml php7-phar php7-intl php7-dom php7-xmlreader php7-ctype php7-session \
    php7-mbstring php7-gd nginx supervisor curl vim bash 
RUN mkdir -p $SUPERVISOR_CONF_DIR
COPY config/supervisor-mysql.ini $SUPERVISOR_CONF_DIR
COPY config/supervisor-nginx.ini $SUPERVISOR_CONF_DIR
COPY config/supervisor-php-fpm.ini $SUPERVISOR_CONF_DIR 
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY config/nginx.conf /etc/nginx/nginx.conf
RUN rm /etc/nginx/conf.d/default.conf
COPY config/fpm-pool.conf /etc/php7/php-fpm.d/www.conf
COPY config/php.ini /etc/php7/conf.d/custom.ini
RUN mkdir -p $MYSQL_DATA_DIR
RUN mysql_install_db --user=$MYSQL_USER --datadir=$MYSQL_DATA_DIR
RUN touch $MYSQL_STDOUT_FILE $MYSQL_STDERR_FILE
RUN mkdir -p /var/www/html
RUN chown -R nobody.nobody /var/www/html && \
  chown -R nobody.nobody /run && \
  chown -R nobody.nobody /var/lib/nginx && \
  chown -R nobody.nobody /var/log/nginx
VOLUME /var/www/html
VOLUME $MYSQL_DATA_DIR
WORKDIR /var/www/html
COPY --chown=nobody codebase/ /var/www/html/

EXPOSE 8080 3306 

# CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
# ENTRYPOINT supervisord -c "/etc/supervisor/conf.d/supervisord.conf"
